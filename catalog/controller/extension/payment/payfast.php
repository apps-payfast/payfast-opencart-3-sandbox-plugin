<?php

class ControllerExtensionPaymentPayfast extends Controller
{

    public function index()
    {
        $this->load->model('checkout/order');

        $this->load->language('extension/payment/payfast');

        $order_id = $this->session->data['order_id'];

        /**
         * get token first
         */
        $merchantid = $this->config->get('payment_payfast_merchant_id');
        $securitykey = $this->config->get('payment_payfast_security_key');


        $data['button_confirm'] = $this->language->get('button_confirm');

        $data['action'] = 'https://ipguat.apps.net.pk/Ecommerce/api/Transaction/PostTransaction';

        $data['MERCHANT_ID'] = $this->config->get('payment_payfast_merchant_id');
        $data['MERCHANT_NAME'] = $this->config->get('payment_payfast_merchant_name');

        $data['BASKET_ID'] = $this->session->data['order_id'];

        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
        $data['TXNAMT'] = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false);
        $authtoken = $this->getPayFastAuthToken(
            $merchantid,
            $securitykey,
            $data['TXNAMT'],
            $this->session->data['order_id'],
            $order_info['currency_code']
        );

        $data['TOKEN'] = $authtoken;
        $data['CURRENCY_CODE'] = $order_info['currency_code'];

        $data['CUSTOMER_EMAIL_ADDRESS'] = $order_info['email'];
        $data['CUSTOMER_MOBILE_NUMBER'] = $order_info['telephone'];


        $signature = md5($merchantid . ":" . $securitykey . ":" . $order_info['total'] . ":" . $this->session->data['order_id']);
        $data['SIGNATURE'] = $signature;
        $data['PROCCODE'] = 00;
        $data['APP_PLUGIN'] = "OPENCART";
        $data['VERSION'] = 'OPENCART3-APPS-PAYMENT-1.1';
        $data['TXNDESC'] = "Product Purchased From: " . $this->config->get('config_name');
        $data['ORDER_DATE'] = date('Y-m-d H:i:s', time());
        $data['SUCCESS_URL'] = urlencode($this->url->link('extension/payment/payfast/callback') . "&redirect=Y&signature=" . $signature . "&basket_id=" . $order_id);
        $data['FAILURE_URL'] = urlencode($this->url->link('extension/payment/payfast/callback') . "&redirect=Y&signature=" . $signature . "&basket_id=" . $order_id);
        $data['CHECKOUT_URL'] = urlencode($this->url->link('extension/payment/payfast/ipncallback') . "&signature=" . $signature . "&basket_id=" . $order_id);


        $data['CUSTOMER_NAME'] = sprintf("%s %s", $order_info['firstname'], $order_info['lastname']);
        $data['CUSTOMER_IPADDRESS'] = $_SERVER['REMOTE_ADDR'];
        $data['COUNTRY'] = $order_info['payment_country'];
        $data['SHIPPING_CUSTOMER_NAME'] = sprintf("%s %s", $order_info['shipping_firstname'], $order_info['shipping_lastname']);
        $data['SHIPPING_ADDRESS_1'] = $order_info['shipping_address_1'];
        $data['SHIPPING_ADDRESS_2'] = $order_info['shipping_address_2'];
        $data['SHIPPING_ADDRESS_CITY'] = $order_info['shipping_city'];
        $data['SHIPPING_STATE_PROVINCE'] = $order_info['shipping_zone'];
        $data['SHIPPING_POSTALCODE'] = $order_info['shipping_postcode'];
        $data['SHIPPING_METHOD'] = $order_info['shipping_method'];
        $data['SHIPPING_COUNTRY'] = $order_info['shipping_country'];
        $data['BILLING_CUSTOMER_NAME'] = sprintf("%s %s", $order_info['payment_firstname'], $order_info['payment_lastname']);
        $data['BILLING_ADDRESS_CITY'] = $order_info['payment_city'];
        $data['BILLING_ADDRESS_1'] = $order_info['payment_address_1'];
        $data['BILLING_ADDRESS_2'] = $order_info['payment_address_2'];
        $data['BILLING_STATE_PROVINCE'] = $order_info['payment_zone'];
        $data['BILLING_POSTALCODE'] = $order_info['payment_postcode'];
        $data['BILLING_COUNTRY'] = $order_info['payment_country'];
        $data['MERCHANT_USERAGENT'] = $_SERVER['HTTP_USER_AGENT'];

        $cartItems = $this->cart->getProducts();
        $tempCart = [];

        $itemCount = 0;
        foreach ($cartItems as $item) {

            $tempCart[] = sprintf(
                "<INPUT TYPE='HIDDEN' NAME='ITEMS[%d][SKU]' value='%s'>
                <INPUT TYPE='HIDDEN' NAME='ITEMS[%d][NAME]' value='%s'>
                <INPUT TYPE='HIDDEN' NAME='ITEMS[%d][PRICE]' value='%s'>
                <INPUT TYPE='HIDDEN' NAME='ITEMS[%d][QTY]' value='%s'>",
                $itemCount,
                $item['product_id'],
                $itemCount,
                $item['name'],
                $itemCount,
                $item['price'],
                $itemCount,
                $item['quantity']
            );
            $itemCount++;
        }

        $data['CARTITEMS'] = join('', $tempCart);

        $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('config_order_status_id'), true);
        return $this->load->view('extension/payment/payfast', $data);
    }

    public function failure()
    {
        $this->load->language('extension/payment/payfast');

        $data = [];
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $data['txn_details'] = $this->language->get('txn_details');
        $this->response->setOutput($this->load->view('extension/payment/payfast_failure', $data));
    }

    public function callback()
    {

        $this->log->write('[PayFast: Transaction Response]: ' . json_encode($_REQUEST));

        $order_id = null;
        $redirect = $this->request->get['redirect'];
        $validation_hash = $this->request->get['validation_hash'];

        if (isset($this->request->get['basket_id'])) {
            $order_id = $this->request->get['basket_id'];
        } else {
            $order_id = 0;
        }

        if (!$order_id) {

            $this->response->redirect($this->url->link('checkout/checkout', '', true));
            exit;
        }

        $this->load->model('checkout/order');

        $merchantid = $this->config->get('payment_payfast_merchant_id');
        $securitykey = $this->config->get('payment_payfast_security_key');
        $apps_status_msg = $this->request->get['err_msg'];
        $apps_transactionid = $this->request->get['transaction_id'];
        $apps_statuscode = $this->request->get['err_code'];
        $apps_rdv_key = $this->request->get['Rdv_Message_Key'];


        $hashVerfied = $this->calculateVallidationHash(
            $validation_hash,
            $merchantid,
            $securitykey,
            $order_id,
            $apps_statuscode
        );

        if (!$hashVerfied) {
            $this->response->redirect($this->url->link('checkout/checkout', '', true));
            exit;
        }

        $order_info = $this->model_checkout_order->getOrder($order_id);

        if ($order_info) {

            $order_status_id = $order_info['order_status_id'];

            if ($order_status_id !== $this->config->get('config_order_status_id')) {
                if ($redirect == "Y") {
                    $this->response->redirect($this->url->link('checkout/checkout', '', true));
                }
                exit;
            }

            $comment_message = "PayFast Transaction ID: " . $apps_transactionid . "<br>";
            $comment_message .= "Message: " . $apps_status_msg . "<br>";

            if ($apps_statuscode == "000") {
                $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_payfast_pending_status_id'), $comment_message, true);
                if ($redirect == "Y") {
                    $this->response->redirect($this->url->link('checkout/success', '', true));
                }
                exit;
            }

            switch ($apps_statuscode) {
                case "529":
                    $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_payfast_canceled_status_id'), $comment_message, true);
                    break;
                default:
                    $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_payfast_failed_status_id'), $comment_message, true);
                    break;
            }

            if ($redirect == "Y") {
                $this->response->redirect($this->url->link('extension/payment/payfast/failure', '', true));
            }
            exit;
        }
    }

    private function getPayFastAuthToken($merchant_id, $security_key, $txnamt, $basket_id, $currency)
    {


        $token_url = sprintf(
            "https://ipguat.apps.net.pk/Ecommerce/api/Transaction/GetAccessToken?MERCHANT_ID=%s&BASKET_ID=%s&TXNAMT=%s&SECURED_KEY=%s&CURRENCY_CODE=%s",
            $merchant_id,
            $basket_id,
            $txnamt,
            $security_key,
            $currency
        );

        $response = $this->payfastCurlRequest($token_url);
        $response_decode = json_decode($response);
        if (isset($response_decode->ACCESS_TOKEN)) {
            return $response_decode->ACCESS_TOKEN;
        }

        return null;
    }

    private function payfastCurlRequest($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'application/json; charset=utf-8    '
        ));
        curl_setopt($ch, CURLOPT_USERAGENT, 'OpenCart 3 PayFast Plugin');
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    private function calculateVallidationHash($validation_hash, $merchantId, $securedKey, $orderId, $errCode)
    {
        $protocol = sprintf(
            "%s|%s|%s|%s",
            $orderId,
            $securedKey,
            $merchantId,
            $errCode
        );

        $hash = hash('sha256', $protocol);
        return $hash == $validation_hash;
    }

    public function ipncallback()
    {

        $this->log->write('[PayFast: Transaction Response]: ' . json_encode($_REQUEST));

        $order_id = null;
        $validation_hash = $this->request->post['validation_hash'];
        $payment_name = $this->request->post['PaymentName'];

        if (isset($this->request->post['basket_id'])) {
            $order_id = $this->request->post['basket_id'];
        } else {
            $order_id = 0;
        }

        if (!$order_id) {
            header("Status: 404 Not Found");
            echo "Order ID Not Found";
            exit;
        }

        $this->load->model('checkout/order');

        $merchantid = $this->config->get('payment_payfast_merchant_id');
        $securitykey = $this->config->get('payment_payfast_security_key');
        $apps_status_msg = $this->request->post['err_msg'];
        $apps_transactionid = $this->request->post['transaction_id'];
        $apps_statuscode = $this->request->post['err_code'];

        $hashVerfied = $this->calculateVallidationHash(
            $validation_hash,
            $merchantid,
            $securitykey,
            $order_id,
            $apps_statuscode
        );

        if (!$hashVerfied) {
            header("Status: 401 Unauthorized");
            echo "Hash could not be validated";
            exit;
        }

        $order_info = $this->model_checkout_order->getOrder($order_id);

        if ($order_info) {

            $order_status_id = $order_info['order_status_id'];
            if ($order_status_id !== $this->config->get('config_order_status_id')) {
                header("Status: 409 Conflict");
                echo "Order already updated";
                exit;
            }

            $comment_message = "PayFast Transaction ID: " . $apps_transactionid . "<br>";
            $comment_message .= "Message: " . $apps_status_msg . "<br>";
            $comment_message .= "Payment method: : " . $payment_name . "<br>";

            if ($apps_statuscode == "000") {
                $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_payfast_pending_status_id'), $comment_message, true);
                echo "Successfully placed an order after successfull transaction.";
                exit;
            }

            switch ($apps_statuscode) {
                case "529":
                    $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_payfast_canceled_status_id'), $comment_message, true);
                    break;
                default:
                    $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_payfast_failed_status_id'), $comment_message, true);
                    break;
            }
            exit;
        }
    }
}
